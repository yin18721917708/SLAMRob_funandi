echo "Building ORB_SLAM2 ROS nodes"

cd Examples/ROS/ORB_SLAM2
mkdir build
cd build
cmake .. -DROS_BUILD_TYPE=Release
make -j4


echo "Building 3d_rrt ROS nodes"
cd ../../3d_rrt
mkdir build
cd build
cmake .. -DROS_BUILD_TYPE=Release
make -j4
